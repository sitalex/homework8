<?php

class Department extends DBEntity
{
    protected $id;
    protected $title;
    protected $phone;

    public function __construct($title, $phone)
    {
        $this->title = htmlspecialchars($title);
        $this->phone = htmlspecialchars($phone);
    }

    static public function getDepartment($id)
    {
        parent::dbs();
        try
        {
            $sql       = 'SELECT * FROM department WHERE id=:id';
            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $departmentArr = $statement->fetchAll();
            $departmentArr = $departmentArr[0];
            $departmentObj = new self($departmentArr['title'], $departmentArr['phone']);
            $departmentObj->setId($departmentArr['id']);

            return $departmentObj;
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    static public function createDepartment($title, $phone)
    {
        $department = new self($title, $phone);
        $department->create();
    }

    public function create()
    {
        parent::dbs();
        try
        {
            $sql = 'INSERT INTO department SET
            title = :title,
            phone = :phone
            ';

            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':phone', $this->phone);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    static public function delete($id)
    {
        $department = self::getDepartment($id);
        $department->destroy();
    }

    public function destroy()
    {
        parent::dbs();
        try
        {
            $sql       = "DELETE FROM department WHERE id=:id";
            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    static public function all()
    {
        parent::dbs();
        try
        {
            $sql         = "SELECT * FROM department";
            $department     = parent::$dbs->query($sql);
            $departmentsArr = $department->fetchAll();
            $departmentObjs = [];
            foreach ($departmentsArr as $departmentArr)
            {
                $departmentObj = new self($departmentArr['title'], $departmentArr['phone']);
                $departmentObj->setId($departmentArr['id']);
                $departmentObjs[] = $departmentObj;
            }

            return $departmentObjs;
        }
        catch (Exception $exception)
        {
            die();
        }
    }
    static public function update($id, $title, $phone)
    {
        $department = new self($title, $phone);
        $department->setId($id);
        $department->edit();
    }

    public function edit()
    {
        parent::dbs();
        try
        {
            $sql = 'UPDATE department SET
            title = :title,
            phone = :phone
            WHERE id=:id';

            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':phone', $this->phone);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    public function setId($id)
    {
        $this->id = htmlspecialchars($id);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getPhone()
    {
        return $this->phone;
    }



}