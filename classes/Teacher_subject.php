<?php


class Teacher_subject extends DBEntity
{

    public static function getSubject($id)
    {
        parent::dbs();
        $sql      = 'SELECT * FROM teacher_subject as ts
        INNER JOIN teacher as t on teacher_id = t.id 
        INNER JOIN subject AS s ON subject_id = s.id where ts.subject_id = :id';
        $variable = parent::$dbs->prepare($sql);
        $variable->bindValue(':id', $id);
        $variable->execute();

        return $variable->fetchAll();
    }

    public static function getTeacher($id)
    {
        parent::dbs();
        $sql      = 'SELECT * FROM teacher_subject as ts
        INNER JOIN subject AS s ON subject_id = s.id
        INNER JOIN teacher as t on teacher_id = t.id where ts.teacher_id = :id';
        $variable = parent::$dbs->prepare($sql);
        $variable->bindValue(':id', $id);
        $variable->execute();

        return $variable->fetchAll();

    }

}