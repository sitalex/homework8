<?php

class Teacher extends DBEntity
{

    protected $id      = 0;
    protected $name    = '';
    protected $surname = '';
    protected $email   = '';
    protected $department_id;
    protected $subjects;


    public function __construct($name, $surname, $email, $department_id = null, $subjects = [])
    {
        $this->name          = htmlspecialchars($name);
        $this->surname       = htmlspecialchars($surname);
        $this->email         = htmlspecialchars($email);
        $this->department_id = htmlspecialchars($department_id);
        $this->subjects      = $subjects;
    }

    public function getAllSubjects()
    {
        parent::dbs();
        try
        {
            $sql       = "SELECT * FROM teacher_subject WHERE teacher_id = :id";
            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->execute();

            $subjects = $statement->fetchAll();
            $subject  = [];
            foreach ($subjects as $value)
            {
                $subject[] = Subject::getSubject($value['subject_id']);
            }

            return $subject;

        }
        catch (Exception $exception)
        {
            die();
        }

    }


    static public function getTeacher($id)
    {
        parent::dbs();
        try
        {
            $sql       = 'SELECT * FROM teacher WHERE id=:id';
            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $teacherArr = $statement->fetchAll();
            $teacherArr = $teacherArr[0];
            $teacherObj = new self
            (
                $teacherArr['name'],
                $teacherArr['surname'],
                $teacherArr['email'],
                $teacherArr['department_id']
            );
            $teacherObj->setId($teacherArr['id']);

            return $teacherObj;
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    static public function delete($id)
    {
        $teacher = self::getTeacher($id);
        $teacher->destroy();
    }

    public function destroy()
    {
        parent::dbs();
        try
        {
            $sql       = "DELETE FROM teacher WHERE id=:id";
            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    static public function createTeacher($name, $surname, $email, $department_id, $subjects)
    {
        $teacher = new self($name, $surname, $email, $department_id, $subjects);
        $teacher->create();
    }

    public function create()
    {
        parent::dbs();
        try
        {
            $sql = 'INSERT INTO teacher SET
            name = :name,
            surname = :surname,
            email = :email,
            department_id = :department_id';

            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':name', $this->name);
            $statement->bindValue(':surname', $this->surname);
            $statement->bindValue(':email', $this->email);
            $statement->bindValue(':department_id', $this->department_id);
            $statement->execute();
            $this->id = parent::$dbs->lastInsertId();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }

        try
        {
            $statementTeacherSubject = parent::$dbs->prepare('INSERT INTO teacher_subject SET
            teacher_id = :teacher_id,
            subject_id = :subject_id');

            foreach ($this->subjects as $subject)
            {
                $statementTeacherSubject->bindValue(':teacher_id', $this->id);
                $statementTeacherSubject->bindValue(':subject_id', $subject);
                $statementTeacherSubject->execute();
            }

        }
        catch (Exception $exception)
        {
            echo "Ошибка!" . $exception->getCode() . ' сообщение: ' . $exception->getMessage();
            die();
        }


    }

    static public function all()
    {
        parent::dbs();
        try
        {
            $sql         = "SELECT * FROM teacher";
            $teacher     = parent::$dbs->query($sql);
            $teachersArr = $teacher->fetchAll();
            $teacherObjs = [];
            foreach ($teachersArr as $teacherArr)
            {
                $teacherObj = new self($teacherArr['name'], $teacherArr['surname'], $teacherArr['email']);
                $teacherObj->setId($teacherArr['id']);
                $teacherObjs[] = $teacherObj;
            }


            return $teacherObjs;
        }
        catch (Exception $exception)
        {
            die();
        }
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = htmlspecialchars($id);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getDepartmentId()
    {
        return $this->department_id;
    }

}