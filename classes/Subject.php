<?php

class Subject extends DBEntity
{
    protected $id;
    protected $title;

    public function __construct($title)
    {
        $this->title = htmlspecialchars($title);
    }

    static public function createSubject($title)
    {
        $subject = new self($title);
        $subject->create();
    }

    static public function all()
    {
        parent::dbs();
        try
        {
            $sql         = "SELECT * FROM subject";
            $subject     = parent::$dbs->query($sql);
            $subjectsArr = $subject->fetchAll();
            $subjectObjs = [];
            foreach ($subjectsArr as $subjectArr)
            {
                $subjectObj = new self($subjectArr['title']);
                $subjectObj->setId($subjectArr['id']);
                $subjectObjs[] = $subjectObj;
            }

            return $subjectObjs;
        }
        catch (Exception $exception)
        {
            die();
        }
    }

    static public function getSubject($id)
    {
        parent::dbs();
        try
        {
            $sql       = 'SELECT * FROM subject WHERE id=:id';
            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $subjectArr = $statement->fetchAll();
            $subjectArr = $subjectArr[0];
            $subjectObj = new self($subjectArr['title']);
            $subjectObj->setId($subjectArr['id']);

            return $subjectObj;
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }
    static public function delete($id)
    {
        $subject = self::getSubject($id);
        $subject->destroy();
    }

    public function destroy()
    {
        parent::dbs();
        try
        {
            $sql       = "DELETE FROM subject WHERE id=:id";
            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    static public function update($id, $title)
    {
        $subject = new self($title);
        $subject->setId($id);
        $subject->edit();
    }

    public function edit()
    {
        parent::dbs();
        try
        {
            $sql = 'UPDATE subject SET
            title = :title
            WHERE id=:id';

            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }
    public function create()
    {
        parent::dbs();
        try
        {
            $sql = 'INSERT INTO subject SET
            title = :title
            ';

            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = htmlspecialchars($id);
    }

    public function getTitle()
    {
        return $this->title;
    }


}