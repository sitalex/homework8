<?php
require_once 'require.php';
$depart   = Department::all();
$subj     = Subject::all();
$teachers = Teacher::all();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p>
    <a href="department/department.php">Department</a>
    <br>
    <a href="subject/subject.php">Create subject</a>
    <br>
</p>
<?php if (!empty($depart && $subj)): ?>
    <h1>Создай своего преподавателя!</h1>
    <form action="teacher/createTeacher.php" method="post">
        <p>
            Кафедра:
            <select name="department">
                <option disabled>Выберите кафедру</option>
                <?php foreach ($depart as $value): ?>
                    <option value="<?= $value->getId() ?>"><?= $value->getTitle() ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        name:
        <br>
        <input type="text" name="name">
        <br>
        surname:
        <br>
        <input type="text" name="surname">
        <br>
        email:
        <br>
        <input type="email" name="email">
        <br>
        <p>
            <select multiple name="subject[]">
                <?php foreach ($subj as $valueSubject): ?>
                    <option value="<?= $valueSubject->getId() ?>"><?= $valueSubject->getTitle() ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        <button>Отправить</button>
    </form>
<h1>Предметы </h1>
    <table>
        <tr>
            <td>title</td>
            <td></td>
        </tr>
        <?php foreach ($subj as $subject): ?>
        <tr>
            <td>
                <?= $subject->getTitle() ?>
            </td>
            <td>
                <a href="subject/amountTeacher.php?id=<?= $subject->getId() ?>">amount teacher</a>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
<h1>Преподаватели </h1>
    <table>
        <tr>
            <th>
                name
            </th>
            <th>
                surname
            </th>
            <th>
            </th>
            <th>
            </th>
        </tr>
        <?php foreach ($teachers as $teacher): ?>
            <tr>
                <td>
                    <?= $teacher->getName() ?>
                </td>
                <td>
                    <?= $teacher->getSurname() ?>
                </td>
                <td>
                    <a href="teacher/viewTeacher.php?id=<?= $teacher->getId() ?>">more</a>
                </td>
                <td>
                    <a href="teacher/deleteTeacher.php?id=<?= $teacher->getId() ?>">delete</a>
                </td>
                <td>
                    <a href="teacher/amountSubject.php?id=<?= $teacher->getId() ?>">amount subject</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>

</body>
</html>
