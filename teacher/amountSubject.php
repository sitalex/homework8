<?php
require_once '../require.php';
if (!empty($_GET['id']))
{
    $TS = Teacher_subject::getTeacher($_GET['id']);
    $teacher = Teacher::getTeacher($_GET['id']);
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1><?= $TS[0]['name'] ?> <?= $TS[0]['surname'] ?></h1>
<table>
    <tr>
        <th>Subject</th>
    </tr>
    <?php foreach ($TS as $value): ?>
        <tr>
            <td>
                <?= $value['title'] ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<br>
<a href="/main.php">main</a>
</body>
</html>
