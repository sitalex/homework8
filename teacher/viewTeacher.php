<?php
require_once '../require.php';

if (!empty($_GET['id']))
{
    $teacher = Teacher::getTeacher($_GET['id']);
    $depart  = Department::getDepartment($teacher->getDepartmentId());
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table>
    <tr>
        <th>
            name
        </th>
        <th>
            surname
        </th>
        <th>
            email
        </th>
        <th>
            subject
        </th>
        <th>
            department
        </th>
        <th>
            phone department
        </th>
    </tr>
    <tr>
        <td><?= $teacher->getName() ?></td>
        <td><?= $teacher->getSurname() ?></td>
        <td><?= $teacher->getEmail() ?></td>
        <td>
        <?php foreach ($teacher->getAllSubjects() as $subject): ?>
            <?= $subject->getTitle() ?><br>
        <?php endforeach; ?>
        </td>
        <td><?= $depart->getTitle() ?></td>
        <td><?= $depart->getPhone() ?></td>
    </tr>
</table>
<a href="../main.php">return</a>
</body>
</html>
