<?php

require_once 'db/db.php';

try
{

    $sqlDepartment = "CREATE TABLE department (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL UNIQUE,
    phone VARCHAR(255) NOT NULL) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($sqlDepartment);

    $sqlTeacher = "CREATE TABLE teacher (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    surname VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    department_id INT,
    FOREIGN KEY (department_id) REFERENCES department (id) ON DELETE CASCADE) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($sqlTeacher);

    $sqlSubject = "CREATE TABLE subject (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL UNIQUE ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($sqlSubject);

    $sql = "CREATE TABLE teacher_subject (
    teacher_id INT,
    subject_id INT,
    FOREIGN KEY (teacher_id) REFERENCES teacher (id) ON DELETE CASCADE,
    FOREIGN KEY (subject_id) REFERENCES subject (id) ON DELETE CASCADE ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($sql);
}
catch (Exception $ex)
{
    header('Location:../main.php');
}
header('Location:../main.php');


