<?php
require_once '../require.php';
$subjects = Subject::all();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Create subject</h1>
<form action="createSubject.php" method="post">
    <input type="text" name="title">
    <button>Отправить</button>
</form>
<table>
    <tr>
        <th>
            title
        </th>
        <th>
        </th>
        <th>
        </th>
    </tr>
    <?php foreach ($subjects as $subject): ?>
        <tr>
            <td><?= $subject->getTitle() ?></td>
            <td><a href="editSubject.php?id=<?= $subject->getId() ?>">update</a></td>
            <td><a href="deleteSubject.php?id=<?= $subject->getId() ?>">delete</a></td>
        </tr>
    <?php endforeach; ?>
</table>
<a href="../main.php">return</a>
</body>
</html>
