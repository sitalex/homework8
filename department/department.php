<?php
require_once '../require.php';
$departments = Department::all();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Create department</h1>
<form action="createDepartment.php" method="post">
    title:<input name="title" type="text">
    tel:<input name="phone" type="tel">
    <button>отправить</button>
</form>
<table>
    <tr>
        <th>
            title
        </th>
        <th>
        </th>
        <th>
        </th>
        <th>
        </th>
    </tr>
    <?php foreach ($departments as $department): ?>
        <tr>
            <td><?= $department->getTitle() ?></td>
            <td><a href="viewDepartment.php?id=<?= $department->getId() ?>">more</a></td>
            <td><a href="editDepartment.php?id=<?= $department->getId() ?>">update</a></td>
            <td><a href="deleteDepartment.php?id=<?= $department->getId() ?>">delete</a></td>
        </tr>
    <?php endforeach; ?>
</table>
<br>
<a href="../main.php">main page</a>

</body>
</html>
