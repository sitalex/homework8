<?php

require_once '../require.php';

if (!empty($_GET['id']))
{
    $department = Department::getDepartment($_GET['id']);
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table>
    <tr>
        <th>
            title
        </th>
        <th>
            phone
        </th>
    </tr>
    <tr>
        <td><?= $department->getTitle() ?></td>
        <td><?= $department->getPhone() ?></td>
    </tr>
</table>
<a href="department.php">return</a>
</body>
</html>
