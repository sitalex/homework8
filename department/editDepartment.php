<?php
require_once '../require.php';

if (!empty($_GET['id']))
{
    $department = Department::getDepartment($_GET['id']);
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>Update department</h1>
<form action="updateDepartment.php" method="post">
    <input type="hidden" name="id" value="<?= $department->getId() ?>">
    title:<input name="title" type="text" value="<?= $department->getTitle() ?>">
    tel:<input name="phone" type="tel" value="<?= $department->getPhone() ?>">
    <button>отправить</button>
</form>
</body>
</html>
